package org.fundacion_jala.notifications_service.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import javax.mail.internet.MimeMessage;
import org.fundacion_jala.notifications_service.models.EmailNotification;
import org.fundacion_jala.notifications_service.models.EmailNotificationDetails;
import org.fundacion_jala.notifications_service.models.enums.NotificationStatus;
import org.fundacion_jala.notifications_service.models.enums.NotificationType;
import org.fundacion_jala.notifications_service.repositories.EmailNotificationRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.javamail.JavaMailSender;
import org.thymeleaf.spring5.SpringTemplateEngine;

@ExtendWith(MockitoExtension.class)
public class EmailServiceImplTest {

  @Mock private EmailNotificationRepository emailNotificationRepository;

  @Mock private JavaMailSender emailSender;

  @Mock private SpringTemplateEngine templateEngine;

  @InjectMocks private EmailServiceImpl emailService;

  private EmailNotification emailNotification;

  private MimeMessage mimeMessage;

  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);
    emailNotification = buildEmailNotification();
  }

  @AfterEach
  public void clear() {
    Mockito.reset(emailNotificationRepository);
  }

  @Test
  void testShouldReturnANotificationEmailOnSave() {
    when(emailNotificationRepository.save(any(EmailNotification.class)))
        .thenReturn(buildEmailNotification());
    EmailNotification emailNotification = emailService.save(buildEmailNotification());
    assertEquals(emailNotification, buildEmailNotification());
    verify(emailNotificationRepository, times(1)).save(buildEmailNotification());
  }

  public EmailNotification buildEmailNotification() {
    EmailNotification response = new EmailNotification();
    Set<String> targets = new HashSet<>();
    Set<EmailNotificationDetails> details = new HashSet<>();
    details.add(buildEmailNotificationDetail());
    targets.add("test@test.com");
    response.setEmailTargets(targets);
    response.setNotificationDetails(details);
    response.setOwnerId(UUID.fromString("045e9370-0dc9-4665-9a89-1435eb7071b5"));
    response.setSender("sender@test.com");
    response.setSubject("testing email from applicants tracking");
    response.setBody("Testing body for notification service - email works");
    response.setType(NotificationType.RESET_PASSWORD);
    response.setStatus(NotificationStatus.SENT_SUCCESSFULLY);
    response.setId(UUID.fromString("d7526a7b-f16a-45a3-b15a-ea61bd102c20"));

    return response;
  }

  private EmailNotificationDetails buildEmailNotificationDetail() {
    EmailNotificationDetails emailDetail = new EmailNotificationDetails();
    emailDetail.setTarget("test@test.com");
    emailDetail.setId(UUID.fromString("485f2967-539a-40cf-999d-00320590464a"));
    emailDetail.setStatus(NotificationStatus.SENT_SUCCESSFULLY);
    return emailDetail;
  }
}

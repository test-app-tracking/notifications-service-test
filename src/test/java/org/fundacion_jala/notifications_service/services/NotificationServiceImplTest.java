package org.fundacion_jala.notifications_service.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.fundacion_jala.notifications_service.models.Notification;
import org.fundacion_jala.notifications_service.repositories.NotificationRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class NotificationServiceImplTest {
  @Mock NotificationRepository notificationRepository;

  @InjectMocks NotificationServiceImpl notificationService;

  private static final String USER_UUID = "48f93b8a-fbb9-11eb-9a03-0242ac130003";
  private final Notification notification1 =
      new Notification(UUID.fromString(USER_UUID), "event title 1", "event description 1", "event");
  private final Notification notification2 =
      new Notification(UUID.fromString(USER_UUID), "event title 2", "event description 2", "event");

  @Test
  public void testShouldReturnANotificationOnSave() {
    when(notificationRepository.save(any(Notification.class))).thenReturn(notification1);

    Notification notificationSaved = notificationService.save(notification1);
    assertEquals(notificationSaved, notification1);
    verify(notificationRepository, times(1)).save(notification1);
  }

  @Test
  public void testShouldReturnListOfNotificationsByUserId() {
    List<Notification> notificationList = new ArrayList<>();
    notificationList.add(notification1);
    notificationList.add(notification2);

    when(notificationRepository.findByUserIdOrderByCreatedAtDesc(UUID.fromString(USER_UUID)))
        .thenReturn(notificationList);

    List<Notification> notificationListReceived =
        notificationService.getAllByUserId(UUID.fromString(USER_UUID));
    assertEquals(notificationListReceived.size(), notificationList.size());
    verify(notificationRepository, times(1))
        .findByUserIdOrderByCreatedAtDesc(UUID.fromString(USER_UUID));
  }
}

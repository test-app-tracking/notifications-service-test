package org.fundacion_jala.notifications_service.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import org.fundacion_jala.notifications_service.models.EmailNotification;
import org.fundacion_jala.notifications_service.models.EmailNotificationDetails;
import org.fundacion_jala.notifications_service.models.enums.NotificationStatus;
import org.fundacion_jala.notifications_service.models.enums.NotificationType;
import org.fundacion_jala.notifications_service.services.EmailServiceImpl;
import org.fundacion_jala.notifications_service.utils.constants.EndPoint;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@WebMvcTest(EmailNotificationController.class)
@AutoConfigureMockMvc
public class EmailNotificationControllerTest {

  @MockBean protected EmailServiceImpl emailService;

  @Autowired protected MockMvc mockMvc;

  private static final String EMAIL_NOTIFICATION_BODY_TO_RESET_PASSWORD =
      "{\n"
          + "    \"emailTargets\": [\n"
          + "        \"name.lastname@fundacion-jala.org\"\n"
          + "    ],\n"
          + "    \"subject\": \"Reset password\",\n"
          + "    \"body\": \"your reset code is 123\",\n"
          + "    \"ownerId\": \"485f2967-539a-40cf-999d-00320590464a\",\n"
          + "    \"type\": \"RESET_PASSWORD\",\n"
          + "    \"hasTemplate\": true\n"
          + "}";

  private static final String EMAIL_NOTIFICATION_BODY_WITHOUT_TARGETS =
      "{\n"
          + "    \"emailTargets\": [],"
          + "    \"subject\": \"Reset password\",\n"
          + "    \"body\": \"your reset code is 123\",\n"
          + "    \"ownerId\": \"485f2967-539a-40cf-999d-00320590464a\",\n"
          + "    \"type\": \"RESET_PASSWORD\",\n"
          + "    \"hasTemplate\": true\n"
          + "}";

  @Test
  public void testSendEmail_thenReturnSuccessMessage() throws Exception {
    EmailNotification notification = buildEmailNotification();
    when(emailService.send(any(EmailNotification.class))).thenReturn(notification);

    mockMvc
        .perform(
            MockMvcRequestBuilders.post(EndPoint.NOTIFICATIONS.concat(EndPoint.EMAIL))
                .content(EMAIL_NOTIFICATION_BODY_TO_RESET_PASSWORD)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void testSendEmail_thenReturnBadRequest() throws Exception {
    EmailNotification notification = buildEmailNotification();
    when(emailService.send(any(EmailNotification.class))).thenReturn(notification);

    mockMvc
        .perform(
            MockMvcRequestBuilders.post(EndPoint.NOTIFICATIONS.concat(EndPoint.EMAIL))
                .content(EMAIL_NOTIFICATION_BODY_WITHOUT_TARGETS)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testGetAllEmailNotifications_thenReturnOkStatus() throws Exception {
    mockMvc
        .perform(
            MockMvcRequestBuilders.get(EndPoint.NOTIFICATIONS.concat(EndPoint.EMAIL))
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void testGetNotificationById_thenReturnOkStatus() throws Exception {
    EmailNotification notification = buildEmailNotification();
    when(emailService.getById(any(UUID.class))).thenReturn(notification);

    mockMvc
        .perform(
            MockMvcRequestBuilders.get(
                    EndPoint.NOTIFICATIONS.concat("/d7526a7b-f16a-45a3-b15a-ea61bd102c20"))
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  public EmailNotification buildEmailNotification() {
    EmailNotification response = new EmailNotification();
    Set<String> targets = new HashSet<>();
    Set<EmailNotificationDetails> details = new HashSet<>();
    details.add(buildEmailNotificationDetail());
    targets.add("test@test.com");
    response.setEmailTargets(targets);
    response.setNotificationDetails(details);
    response.setOwnerId(UUID.fromString("045e9370-0dc9-4665-9a89-1435eb7071b5"));
    response.setSender("sender@test.com");
    response.setSubject("testing email from applicants tracking");
    response.setBody("Testing body for notification service - email works");
    response.setType(NotificationType.RESET_PASSWORD);
    response.setStatus(NotificationStatus.SENT_SUCCESSFULLY);
    response.setId(UUID.fromString("d7526a7b-f16a-45a3-b15a-ea61bd102c20"));
    response.getNotificationDetails().forEach(detail -> detail.setOwner(response));
    return response;
  }

  private EmailNotificationDetails buildEmailNotificationDetail() {
    EmailNotificationDetails emailDetail = new EmailNotificationDetails();
    emailDetail.setTarget("test@test.com");
    emailDetail.setId(UUID.fromString("485f2967-539a-40cf-999d-00320590464a"));
    emailDetail.setStatus(NotificationStatus.SENT_SUCCESSFULLY);
    return emailDetail;
  }
}

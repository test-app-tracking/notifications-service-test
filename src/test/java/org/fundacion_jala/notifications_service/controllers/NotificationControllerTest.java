package org.fundacion_jala.notifications_service.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.fundacion_jala.notifications_service.models.Notification;
import org.fundacion_jala.notifications_service.services.NotificationWebService;
import org.fundacion_jala.notifications_service.utils.constants.EndPoint;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@WebMvcTest(NotificationController.class)
@AutoConfigureMockMvc
public class NotificationControllerTest {
  @MockBean private NotificationWebService notificationWebService;

  @Autowired protected MockMvc mockMvc;

  @Autowired private ObjectMapper mapper;

  private static final String USER_PATH = "/user/";
  private static final String USER_UUID = "48f93b8a-fbb9-11eb-9a03-0242ac130003";
  private static final String RESPONSE_SUCCESS_SEND_NOTIFICATION_MESSAGE =
      "Notifications sent successfully.";

  private final Notification notification1 =
      new Notification(UUID.fromString(USER_UUID), "event title 1", "event description 1", "event");
  private final Notification notification2 =
      new Notification(UUID.fromString(USER_UUID), "event title 2", "event description 2", "event");
  List<Notification> notificationList = new ArrayList<>();

  @BeforeEach
  public void setUp() {
    notificationList.add(notification1);
    notificationList.add(notification2);
  }

  @Test
  public void testGetNotificationsOfUser_expectedListOfNotifications() throws Exception {

    given(notificationWebService.getAllByUserId(any(UUID.class))).willReturn(notificationList);
    mockMvc
        .perform(
            MockMvcRequestBuilders.get(EndPoint.NOTIFICATIONS.concat(USER_PATH).concat(USER_UUID)))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[0].title", Matchers.is(notification1.getTitle())));
  }

  @Test
  public void testSendNotification_expectedResponse() throws Exception {
    mockMvc
        .perform(
            MockMvcRequestBuilders.post(EndPoint.NOTIFICATIONS.concat(EndPoint.SEND_NOTIFICATION))
                .content(mapper.writeValueAsString(notificationList))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.message", Matchers.is(RESPONSE_SUCCESS_SEND_NOTIFICATION_MESSAGE)));
  }
}

package org.fundacion_jala.notifications_service.pagination;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.fundacion_jala.notifications_service.models.EmailNotification;

@Getter
@Setter
@Builder
public class DataAndPagination {

  private Pagination pagination;
  private Iterable<EmailNotification> data;
}

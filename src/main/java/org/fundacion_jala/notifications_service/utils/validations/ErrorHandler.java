package org.fundacion_jala.notifications_service.utils.validations;

import java.util.HashMap;
import java.util.Map;
import org.fundacion_jala.notifications_service.utils.exceptions.EntityNotFoundException;
import org.fundacion_jala.notifications_service.utils.exceptions.NotificationNotSentException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ErrorHandler {

  private static final String ATTRIBUTE_NAME_MESSAGE_ERROR = "error: ";

  @ExceptionHandler(NotificationNotSentException.class)
  public ResponseEntity<Map<String, Object>> handleEntityNotFoundException(
      NotificationNotSentException exception) {
    Map<String, Object> body = new HashMap<>();
    body.put(ATTRIBUTE_NAME_MESSAGE_ERROR, exception.getMessage());
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(body);
  }

  @ExceptionHandler(EntityNotFoundException.class)
  public ResponseEntity<Map<String, Object>> handleEntityNotFoundException(
      EntityNotFoundException exception) {
    Map<String, Object> body = new HashMap<>();
    body.put(ATTRIBUTE_NAME_MESSAGE_ERROR, exception.getMessage());
    return ResponseEntity.status(HttpStatus.NOT_FOUND).body(body);
  }
}

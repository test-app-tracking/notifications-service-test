package org.fundacion_jala.notifications_service.utils.validations;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.fundacion_jala.notifications_service.utils.constants.EmailConstants;

public class EmailValidator {

  /**
   * Validates if a email format is valid or not. Both the local part and the domain name can
   * contain one or more dots, but no two dots can appear right next to each other. Also, the first
   * and last characters must not be dots.
   *
   * @param email string
   * @return true if it's valid, otherwise it returns false.
   */
  public static boolean isEmailValid(String email) {
    Pattern pattern = Pattern.compile(EmailConstants.EMAIL_REGEX);
    Matcher matcher = pattern.matcher(email);
    return matcher.matches();
  }
}

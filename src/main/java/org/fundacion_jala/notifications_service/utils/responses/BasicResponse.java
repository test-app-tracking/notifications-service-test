package org.fundacion_jala.notifications_service.utils.responses;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BasicResponse {
  private String message;

  public BasicResponse(String message) {
    this.message = message;
  }
}

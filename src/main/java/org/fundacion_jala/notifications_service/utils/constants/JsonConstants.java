package org.fundacion_jala.notifications_service.utils.constants;

public class JsonConstants {

  public static final String PROPERTY_NAME = "type";
  public static final String NAME_RESET_PASSWORD = "reset-password-notification";
  public static final String NAME_NEW_USERS = "new-users-notification";
}

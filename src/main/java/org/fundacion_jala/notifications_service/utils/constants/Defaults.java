package org.fundacion_jala.notifications_service.utils.constants;

public class Defaults {

  public static final String EMPTY = "";
  public static final String OPENING_BRACE = "{";
  public static final String CLOSING_BRACE = "}";
  public static final String BLANK_SPACE = " ";
  public static final String COMMA = ",";
  public static final String EQUALS = "=";
  public static final String DASH = "-";
}

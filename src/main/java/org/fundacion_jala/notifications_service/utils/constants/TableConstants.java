package org.fundacion_jala.notifications_service.utils.constants;

public class TableConstants {

  public static final String EMAIL_NOTIFICATION_TABLE = "email_notification";
  public static final String EMAIL_NOTIFICATION_ENTITY = "owner";
  public static final String TYPE_UUID_CHAR = "uuid-char";
  public static final String EMAIL_TARGETS_TABLE = "email_targets";
  public static final String EMAIL_TARGET_COLUMN = "email_target";
  public static final String EMAIL_NOTIFICATION_ID = "email_notification_id";
  public static final String COLUMN_DEFINITION_DELETED = "boolean default false";
}

package org.fundacion_jala.notifications_service.utils.constants;

public class EmailConstants {

  public static final String DEFAULT_SENDER_VALUE = "${spring.mail.username}";
  public static final String TARGET = "target";
  public static final String BODY = "body";
  public static final String USERS = "users";
  public static final String NEW_USERS_BODY = "New User Notification for Admins and Coordinators";
  public static final String SIGN = "sign";
  public static final String DEFAULT_SIGN = "Applicants Tracking";
  public static final String EMAILING_LOGGER =
      "Emailing to: {},\nfrom: {}, \nsubject: {}, \nbody: {}, \nownerId: {}";
  public static final String AT_SYMBOL = "@";
  public static final String APPLICANTS_TRACKING_EMAIL_TEMPLATE = "applicantsTrackingEmailTemplate";
  public static final String APPLICANTS_TRACKING_RESET_PASSWORD_EMAIL_TEMPLATE =
      "resetPasswordEmailTemplate.html";
  public static final String APPLICANTS_TRACKING_NEW_USERS_EMAIL_TEMPLATE =
      "newUsersEmailTemplate.html";
  public static final String ERROR = "Error: {}";
  public static final int MIN_TARGETS = 1;
  public static final String EMAIL_NOTIFICATION = "email";
  public static final String TEMPLATES_PREFIX = "${thymeleaf.templates.prefix}";
  public static final String HTML_SUFFIX = "${thymeleaf.templates.suffix}";
  public static final String INVALID_EMAIL_FORMAT_ERROR = "Invalid email address format for {}";
  public static final String EMAIL_REGEX =
      "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+(?:\\.[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+)*@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";
  public static final String RESET_PASSWORD_CODE_KEY = "code";
  public static final String RESET_PASSWORD_LINK_KEY = "link";
  public static final String TEMPLATE_VARIABLE_CODE = "code";
  public static final String TEMPLATE_VARIABLE_LINK = "resetLink";
}

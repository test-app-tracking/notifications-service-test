package org.fundacion_jala.notifications_service.utils.constants;

public class SwaggerConstants {
  public static final String API_OPERATION_GET_USER_NOTIFICATIONS_SUMMARY =
      "Get all user notifications by user id";
  public static final String API_OPERATION_SEND_NOTIFICATION_SUMMARY =
      "Send a notification to different users";

  public static final String API_OPERATION_GET_EMAIL_NOTIFICATION_SUMMARY =
      "Get an email notification by notification id";
  public static final String API_OPERATION_GET_EMAIL_NOTIFICATIONS_SUMMARY =
      "Get all email notifications";
  public static final String API_OPERATION_SEND_AN_EMAIL_SUMMARY = "Send an email";
}

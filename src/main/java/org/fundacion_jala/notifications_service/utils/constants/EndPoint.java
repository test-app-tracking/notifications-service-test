package org.fundacion_jala.notifications_service.utils.constants;

public class EndPoint {

  public static final String NOTIFICATION_SERVICE = "/api/v1";
  public static final String NOTIFICATIONS = NOTIFICATION_SERVICE + "/notifications";
  public static final String EMAIL = "/email";
  public static final String ID = "/{notificationId}";
  public static final String NOTIFICATION_ID = "notificationId";
  public static final String USER_NOTIFICATIONS = "/user/{userId}";
  public static final String SEND_NOTIFICATION = "/notify";
  public static final String USER_ID = "userId";
}

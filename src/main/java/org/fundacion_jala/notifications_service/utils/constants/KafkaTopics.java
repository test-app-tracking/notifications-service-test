package org.fundacion_jala.notifications_service.utils.constants;

public class KafkaTopics {

  public static final String KAFKA_CONTAINER_FACTORY = "notificationContainerFactory";
  public static final String ADD_NEW_USERS = "add_new_users";
}

package org.fundacion_jala.notifications_service.utils.exceptions;

public class NotificationNotSentException extends RuntimeException {

  private static final String ERROR_MESSAGE = "Couldn't send %s notification";

  /** Custom exception that is thrown when the notification was not sent */
  public NotificationNotSentException(String notificationType) {
    super(String.format(ERROR_MESSAGE, notificationType));
  }
}

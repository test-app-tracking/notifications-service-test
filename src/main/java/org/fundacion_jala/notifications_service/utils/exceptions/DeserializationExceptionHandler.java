package org.fundacion_jala.notifications_service.utils.exceptions;

import static org.fundacion_jala.notifications_service.utils.constants.Defaults.DASH;

import java.util.List;
import lombok.SneakyThrows;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.SerializationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.listener.ErrorHandler;
import org.springframework.kafka.listener.MessageListenerContainer;

public class DeserializationExceptionHandler implements ErrorHandler {

  private static final String REGEX_ERROR_KEY = "Error deserializing key/value for partition ";
  private static final String REGEX_SEEK_PAST =
      ". If needed, please seek past the record to continue consumption.";
  private static final String REGEX_OFFSET = "offset ";
  private static final String REGEX_AT = " at";
  private static final Logger logger =
      LoggerFactory.getLogger(DeserializationExceptionHandler.class);

  /** {@inheritDoc} */
  @Override
  public void handle(
      Exception thrownException,
      List<ConsumerRecord<?, ?>> records,
      Consumer<?, ?> consumer,
      MessageListenerContainer container) {
    if (thrownException instanceof SerializationException) {
      String topicOffsetAndPartition =
          thrownException.getMessage().split(REGEX_ERROR_KEY)[1].split(REGEX_SEEK_PAST)[0];
      String topic = topicOffsetAndPartition.split(DASH)[0];
      int offset = Integer.parseInt(topicOffsetAndPartition.split(REGEX_OFFSET)[1]);
      int partition = Integer.parseInt(topicOffsetAndPartition.split(DASH)[1].split(REGEX_AT)[0]);

      TopicPartition topicPartition = new TopicPartition(topic, partition);
      logger.info("Skipping " + topic + "-" + partition + " offset " + offset + " corrupt data");
      consumer.seek(topicPartition, offset + 1);
    }
  }

  @SneakyThrows
  @Override
  public void handle(Exception e, ConsumerRecord<?, ?> consumerRecord) {
    throw new UnsupportedMethodException("This method is not implemented");
  }
}

package org.fundacion_jala.notifications_service.utils.exceptions;

public class UnsupportedMethodException extends Exception {

  public UnsupportedMethodException(String errorMessage) {
    super(errorMessage);
  }
}

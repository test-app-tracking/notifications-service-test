package org.fundacion_jala.notifications_service.utils.exceptions;

import java.util.UUID;

public class EntityNotFoundException extends RuntimeException {

  /**
   * Custom exception that throw when a entity is not found.
   *
   * @param type name of the type of model such as User.
   * @param id entity id.
   */
  public EntityNotFoundException(String type, UUID id) {
    super(String.format("%s with id:%s not found", type, id));
  }

  public EntityNotFoundException(String type, String name) {
    super(String.format("%s with name: %s not found", type, name));
  }
}

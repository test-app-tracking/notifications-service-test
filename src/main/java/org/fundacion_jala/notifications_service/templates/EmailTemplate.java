package org.fundacion_jala.notifications_service.templates;

import java.util.Map;
import org.fundacion_jala.notifications_service.models.EmailNotification;
import org.thymeleaf.spring5.SpringTemplateEngine;

public interface EmailTemplate {

  String buildTemplate(
      EmailNotification email, Map<String, Object> variables, SpringTemplateEngine templateEngine);
}

package org.fundacion_jala.notifications_service.templates;

import java.util.Map;
import org.fundacion_jala.notifications_service.models.EmailNotification;
import org.fundacion_jala.notifications_service.utils.constants.EmailConstants;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

public class DefaultEmailTemplate implements EmailTemplate {

  @Override
  public String buildTemplate(
      EmailNotification email, Map<String, Object> variables, SpringTemplateEngine templateEngine) {
    Context context = new Context();
    context.setVariables(variables);
    return templateEngine.process(EmailConstants.APPLICANTS_TRACKING_EMAIL_TEMPLATE, context);
  }
}

package org.fundacion_jala.notifications_service.templates;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;
import org.fundacion_jala.notifications_service.models.EmailNotification;
import org.fundacion_jala.notifications_service.utils.constants.Defaults;
import org.fundacion_jala.notifications_service.utils.constants.EmailConstants;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

public class ResetPasswordTemplate implements EmailTemplate {

  @Override
  public String buildTemplate(
      EmailNotification email, Map<String, Object> variables, SpringTemplateEngine templateEngine) {
    addResetPasswordVariables(variables, email);
    Context context = new Context();
    context.setVariables(variables);
    return templateEngine.process(
        EmailConstants.APPLICANTS_TRACKING_RESET_PASSWORD_EMAIL_TEMPLATE, context);
  }

  private void addResetPasswordVariables(Map<String, Object> variables, EmailNotification email) {
    variables.put(
        EmailConstants.TEMPLATE_VARIABLE_CODE,
        getBodyValues(email.getBody()).get(EmailConstants.RESET_PASSWORD_CODE_KEY));
    variables.put(
        EmailConstants.TEMPLATE_VARIABLE_LINK,
        getBodyValues(email.getBody()).get(EmailConstants.RESET_PASSWORD_LINK_KEY));
  }

  private Map<String, String> getBodyValues(String bodyAsString) {
    bodyAsString = bodyAsString.replace(Defaults.OPENING_BRACE, Defaults.EMPTY);
    bodyAsString = bodyAsString.replace(Defaults.CLOSING_BRACE, Defaults.EMPTY);
    bodyAsString = bodyAsString.replace(Defaults.BLANK_SPACE, Defaults.EMPTY);
    Map<String, String> map =
        Arrays.stream(bodyAsString.split(Defaults.COMMA))
            .map(entry -> entry.split(Defaults.EQUALS))
            .collect(Collectors.toMap(entry -> entry[0], entry -> entry[1]));
    return map;
  }
}

package org.fundacion_jala.notifications_service.templates;

import java.util.EnumMap;
import java.util.Map;
import org.fundacion_jala.notifications_service.models.EmailNotification;
import org.fundacion_jala.notifications_service.models.enums.NotificationType;
import org.thymeleaf.spring5.SpringTemplateEngine;

public class EmailTemplateFactory {

  private static EnumMap<NotificationType, EmailTemplate> factory;

  static {
    factory = new EnumMap<>(NotificationType.class);
    factory.put(NotificationType.GENERATED_DEFAULT_PASSWORD, new DefaultEmailTemplate());
    factory.put(NotificationType.EVENT_NOTIFICATION, new DefaultEmailTemplate());
    factory.put(NotificationType.NEW_USER_NOTIFICATION, new NewUserTemplate());
    factory.put(NotificationType.RESET_PASSWORD, new ResetPasswordTemplate());
  }

  public String getTemplate(
      EmailNotification email, Map<String, Object> variables, SpringTemplateEngine templateEngine) {
    EmailTemplate emailTemplate = factory.get(email.getType());
    return emailTemplate.buildTemplate(email, variables, templateEngine);
  }
}

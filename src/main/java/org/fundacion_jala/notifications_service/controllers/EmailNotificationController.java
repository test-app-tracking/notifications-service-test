package org.fundacion_jala.notifications_service.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.UUID;
import javax.mail.MessagingException;
import javax.validation.Valid;
import org.fundacion_jala.notifications_service.models.EmailNotification;
import org.fundacion_jala.notifications_service.pagination.DataAndPagination;
import org.fundacion_jala.notifications_service.responses.NotificationResponse;
import org.fundacion_jala.notifications_service.services.NotificationService;
import org.fundacion_jala.notifications_service.utils.constants.EmailConstants;
import org.fundacion_jala.notifications_service.utils.constants.EndPoint;
import org.fundacion_jala.notifications_service.utils.constants.SwaggerConstants;
import org.fundacion_jala.notifications_service.utils.exceptions.NotificationNotSentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "Email notification")
@RestController
@RequestMapping(EndPoint.NOTIFICATIONS)
@CrossOrigin(origins = "${react.origin.url}")
public class EmailNotificationController {

  private static final Logger logger = LoggerFactory.getLogger(EmailNotificationController.class);
  private static final String LOGGER_MESSAGE = "Sending email to {} targets";

  @Autowired private NotificationService<EmailNotification> emailService;

  @ApiOperation(value = SwaggerConstants.API_OPERATION_SEND_AN_EMAIL_SUMMARY)
  @PostMapping(EndPoint.EMAIL)
  public ResponseEntity<NotificationResponse> sendEmail(
      @Valid @RequestBody EmailNotification emailNotification) {
    if (emailNotification.getEmailTargets().size() >= EmailConstants.MIN_TARGETS) {
      logger.info(LOGGER_MESSAGE, emailNotification.getEmailTargets().size());

      try {
        EmailNotification notification = emailService.send(emailNotification);
        return ResponseEntity.ok(new NotificationResponse(notification));
      } catch (MessagingException exception) {
        throw new NotificationNotSentException(EmailConstants.EMAIL_NOTIFICATION);
      }
    }
    throw new NotificationNotSentException(EmailConstants.EMAIL_NOTIFICATION);
  }

  @ApiOperation(value = SwaggerConstants.API_OPERATION_GET_EMAIL_NOTIFICATIONS_SUMMARY)
  @GetMapping(EndPoint.EMAIL)
  public ResponseEntity<DataAndPagination> getAllEmailNotifications(
      @RequestParam(defaultValue = "1") Integer page,
      @RequestParam(defaultValue = "10") Integer size,
      @RequestParam(defaultValue = "subject") String sort) {
    return ResponseEntity.ok(emailService.getAllPagination(page, size, sort));
  }

  @ApiOperation(value = SwaggerConstants.API_OPERATION_GET_EMAIL_NOTIFICATION_SUMMARY)
  @GetMapping(EndPoint.ID)
  public ResponseEntity<EmailNotification> getEmailNotificationByID(
      @PathVariable(EndPoint.NOTIFICATION_ID) UUID notificationId) {
    return ResponseEntity.ok(emailService.getById(notificationId));
  }
}

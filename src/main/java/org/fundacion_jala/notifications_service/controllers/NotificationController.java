package org.fundacion_jala.notifications_service.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.UUID;
import org.fundacion_jala.notifications_service.models.Notification;
import org.fundacion_jala.notifications_service.services.NotificationWebService;
import org.fundacion_jala.notifications_service.utils.constants.EndPoint;
import org.fundacion_jala.notifications_service.utils.constants.SwaggerConstants;
import org.fundacion_jala.notifications_service.utils.responses.BasicResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(tags = "Notification")
@RestController
@RequestMapping(EndPoint.NOTIFICATIONS)
@CrossOrigin(origins = "${react.origin.url}")
public class NotificationController {

  private static final Logger logger = LoggerFactory.getLogger(NotificationController.class);
  private static final String GET_USER_NOTIFICATIONS_LOGGER_MESSAGE =
      "Get notification of user with id {}";
  private static final String SEND_NOTIFICATION_LOGGER_MESSAGE = "Sending notifications";
  private static final String SUCCESS_SEND_NOTIFICATION_MESSAGE =
      "Notifications sent successfully.";

  @Autowired private NotificationWebService notificationService;

  @ApiOperation(value = SwaggerConstants.API_OPERATION_GET_USER_NOTIFICATIONS_SUMMARY)
  @GetMapping(EndPoint.USER_NOTIFICATIONS)
  public ResponseEntity<List<Notification>> getUserNotifications(
      @PathVariable(EndPoint.USER_ID) UUID userId) {
    logger.info(GET_USER_NOTIFICATIONS_LOGGER_MESSAGE, userId);
    return ResponseEntity.ok(notificationService.getAllByUserId(userId));
  }

  @ApiOperation(value = SwaggerConstants.API_OPERATION_SEND_NOTIFICATION_SUMMARY)
  @PostMapping(EndPoint.SEND_NOTIFICATION)
  @ResponseBody
  public ResponseEntity<BasicResponse> sendNotification(
      @RequestBody List<Notification> notifications) {
    logger.info(SEND_NOTIFICATION_LOGGER_MESSAGE);
    for (Notification notification : notifications) {
      notificationService.send(notification);
    }
    BasicResponse response = new BasicResponse(SUCCESS_SEND_NOTIFICATION_MESSAGE);
    return ResponseEntity.ok(response);
  }
}

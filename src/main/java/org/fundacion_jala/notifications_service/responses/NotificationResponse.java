package org.fundacion_jala.notifications_service.responses;

import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import org.fundacion_jala.notifications_service.models.EmailNotification;
import org.fundacion_jala.notifications_service.models.enums.NotificationStatus;

@Getter
@Setter
public class NotificationResponse {

  private UUID id;
  private Set<String> targets;
  private String sender;
  private String subject;
  private String body;
  private NotificationStatus status;

  public NotificationResponse(EmailNotification notification) {
    this.id = notification.getId();
    this.targets = notification.getEmailTargets();
    this.sender = notification.getSender();
    this.subject = notification.getSubject();
    this.body = notification.getBody();
    this.status = notification.getStatus();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof NotificationResponse)) {
      return false;
    }
    NotificationResponse that = (NotificationResponse) o;
    return Objects.equals(getId(), that.getId())
        && Objects.equals(getTargets(), that.getTargets())
        && Objects.equals(getSender(), that.getSender())
        && Objects.equals(getSubject(), that.getSubject())
        && Objects.equals(getBody(), that.getBody())
        && getStatus() == that.getStatus();
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, targets, sender, subject, body, status);
  }
}

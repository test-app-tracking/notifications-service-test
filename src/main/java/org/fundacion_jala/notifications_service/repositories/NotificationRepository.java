package org.fundacion_jala.notifications_service.repositories;

import java.util.List;
import java.util.UUID;
import org.fundacion_jala.notifications_service.models.Notification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotificationRepository extends JpaRepository<Notification, UUID> {
  List<Notification> findByUserIdOrderByCreatedAtDesc(UUID userId);
}

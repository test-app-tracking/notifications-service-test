package org.fundacion_jala.notifications_service.repositories;

import java.util.UUID;
import org.fundacion_jala.notifications_service.models.EmailNotification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmailNotificationRepository extends JpaRepository<EmailNotification, UUID> {

  Page<EmailNotification> findByDeleted(Pageable pageable, boolean deleted);
}

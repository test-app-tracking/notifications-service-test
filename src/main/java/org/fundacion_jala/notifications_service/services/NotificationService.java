package org.fundacion_jala.notifications_service.services;

import java.util.UUID;
import javax.mail.MessagingException;
import org.fundacion_jala.notifications_service.pagination.DataAndPagination;

public interface NotificationService<T> {

  T send(T notification) throws MessagingException;

  T save(T email);

  T getById(UUID id);

  DataAndPagination getAllPagination(Integer pageNo, Integer pageSize, String sortBy);
}

package org.fundacion_jala.notifications_service.services;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.fundacion_jala.notifications_service.models.EmailNotification;
import org.fundacion_jala.notifications_service.models.EmailNotificationDetails;
import org.fundacion_jala.notifications_service.models.enums.NotificationStatus;
import org.fundacion_jala.notifications_service.pagination.DataAndPagination;
import org.fundacion_jala.notifications_service.pagination.Pagination;
import org.fundacion_jala.notifications_service.repositories.EmailNotificationRepository;
import org.fundacion_jala.notifications_service.templates.EmailTemplateFactory;
import org.fundacion_jala.notifications_service.utils.constants.EmailConstants;
import org.fundacion_jala.notifications_service.utils.exceptions.EntityNotFoundException;
import org.fundacion_jala.notifications_service.utils.validations.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.mail.MailSendException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.spring5.SpringTemplateEngine;

@Service
public class EmailServiceImpl implements NotificationService<EmailNotification> {

  private static final Logger logger = LoggerFactory.getLogger(EmailServiceImpl.class);
  private static final String TYPE = "Email Notification";
  private static final String ERROR_MESSAGE = "Invalid email address format for %s";

  @Autowired private EmailNotificationRepository emailNotificationRepository;

  @Autowired private JavaMailSender emailSender;

  @Autowired private SpringTemplateEngine templateEngine;

  @Value(EmailConstants.DEFAULT_SENDER_VALUE)
  private String defaultSender;

  @Override
  public EmailNotification save(EmailNotification email) {
    return emailNotificationRepository.save(email);
  }

  @Override
  public EmailNotification getById(UUID id) {
    Optional<EmailNotification> emailNotification = emailNotificationRepository.findById(id);
    if (emailNotification.isPresent()) {
      return emailNotification.get();
    }
    throw new EntityNotFoundException(TYPE, id);
  }

  @Override
  public DataAndPagination getAllPagination(Integer pageNo, Integer pageSize, String sortBy) {
    int pageNoCorrection = pageNo - 1;
    Pageable pageable = PageRequest.of(pageNoCorrection, pageSize, Sort.by(sortBy));
    Page<EmailNotification> page = emailNotificationRepository.findByDeleted(pageable, false);
    int totalPages = page.getTotalPages();
    int nextPageNumber = page.hasNext() ? pageNo + 1 : totalPages;
    int previousPageNumber = page.hasPrevious() ? pageNo - 1 : 0;
    Pagination generatedPagination =
        Pagination.builder()
            .currentPage(pageNo)
            .pageSize(pageSize)
            .totalPages(page.getTotalPages())
            .totalCount((int) page.getTotalElements())
            .hasPreviousPage(page.hasPrevious())
            .hasNextPage(page.hasNext())
            .nextPageNumber(nextPageNumber)
            .previousPageNumber(previousPageNumber)
            .build();
    if (page.hasContent()) {
      return DataAndPagination.builder()
          .pagination(generatedPagination)
          .data(page.getContent())
          .build();
    }
    return DataAndPagination.builder()
        .pagination(generatedPagination)
        .data(Collections.EMPTY_LIST)
        .build();
  }

  @Override
  public EmailNotification send(EmailNotification email) throws MessagingException {
    if (email.getSender() == null || email.getSender().isEmpty()) {
      email.setSender(defaultSender);
    }
    email.setStatus(NotificationStatus.SAVED);
    if (email.isHasTemplate()) {
      email = sendTemplateEmail(email);
    } else {
      email = sendPlainEmail(email);
    }
    return emailNotificationRepository.save(email);
  }

  private EmailNotification sendPlainEmail(EmailNotification email) {
    SimpleMailMessage message = new SimpleMailMessage();
    Set<EmailNotificationDetails> details = new HashSet<>();
    email
        .getEmailTargets()
        .forEach(
            target -> {
              EmailNotificationDetails emailDetail = new EmailNotificationDetails();
              emailDetail.setTarget(target);
              emailDetail.setOwner(email);
              if (EmailValidator.isEmailValid(target)) {
                message.setTo(target);
                message.setFrom(defaultSender);
                message.setSubject(email.getSubject());
                message.setText(email.getBody());
                logger.info(
                    EmailConstants.EMAILING_LOGGER,
                    target,
                    email.getSender(),
                    email.getSubject(),
                    email.getBody(),
                    email.getOwnerId());
                try {
                  emailSender.send(message);
                  emailDetail.setStatus(NotificationStatus.SENT_SUCCESSFULLY);
                } catch (MailSendException exception) {
                  logger.error(EmailConstants.ERROR, exception.getMessage());
                  emailDetail.setStatus(NotificationStatus.FAILED);
                  emailDetail.setMessage(exception.getMessage());
                }
              } else {
                logger.error(EmailConstants.INVALID_EMAIL_FORMAT_ERROR, target);
                emailDetail.setStatus(NotificationStatus.FAILED);
                emailDetail.setMessage(String.format(ERROR_MESSAGE, target));
              }
              details.add(emailDetail);
            });
    email.setNotificationDetails(details);
    email.setStatus(checkStatus(email.getNotificationDetails()));
    return save(email);
  }

  private EmailNotification sendTemplateEmail(EmailNotification email) throws MessagingException {
    EmailTemplateFactory templateFactory = new EmailTemplateFactory();
    Set<EmailNotificationDetails> details = new HashSet<>();
    MimeMessage message = emailSender.createMimeMessage();
    MimeMessageHelper helper =
        new MimeMessageHelper(
            message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
    Map<String, Object> variables = new HashMap<>();
    variables.put(EmailConstants.SIGN, EmailConstants.DEFAULT_SIGN);
    variables.put(EmailConstants.BODY, email.getBody());
    email
        .getEmailTargets()
        .forEach(
            target -> {
              variables.put(
                  EmailConstants.TARGET,
                  target.substring(0, target.lastIndexOf(EmailConstants.AT_SYMBOL)));
              EmailNotificationDetails emailDetail = new EmailNotificationDetails();
              emailDetail.setTarget(target);
              emailDetail.setOwner(email);
              if (EmailValidator.isEmailValid(target)) {
                logger.info(
                    EmailConstants.EMAILING_LOGGER,
                    target,
                    email.getSender(),
                    email.getSubject(),
                    email.getBody(),
                    email.getOwnerId());
                String html = templateFactory.getTemplate(email, variables, templateEngine);
                try {
                  helper.setTo(target);
                  helper.setText(html, true);
                  helper.setSubject(email.getSubject());
                  helper.setFrom(defaultSender);
                  emailSender.send(message);
                  emailDetail.setStatus(NotificationStatus.SENT_SUCCESSFULLY);
                } catch (MessagingException exception) {
                  logger.error(EmailConstants.ERROR, exception.getMessage());
                  emailDetail.setStatus(NotificationStatus.FAILED);
                  emailDetail.setMessage(exception.getMessage());
                }
              } else {
                logger.error(EmailConstants.INVALID_EMAIL_FORMAT_ERROR, target);
                emailDetail.setStatus(NotificationStatus.FAILED);
                emailDetail.setMessage(String.format(ERROR_MESSAGE, target));
              }
              details.add(emailDetail);
            });
    email.setNotificationDetails(details);
    email.setStatus(checkStatus(email.getNotificationDetails()));
    return save(email);
  }

  private NotificationStatus checkStatus(Set<EmailNotificationDetails> notificationDetails) {
    int errorCount = 0;
    for (EmailNotificationDetails detail : notificationDetails) {
      if (detail.getStatus() == NotificationStatus.FAILED) {
        errorCount++;
      }
    }

    if (errorCount == 0) {
      return NotificationStatus.SENT_SUCCESSFULLY;
    } else if (errorCount == notificationDetails.size()) {
      return NotificationStatus.FAILED;
    } else {
      return NotificationStatus.SENT_WITH_ERRORS;
    }
  }
}

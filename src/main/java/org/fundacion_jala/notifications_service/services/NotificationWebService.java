package org.fundacion_jala.notifications_service.services;

import java.util.List;
import java.util.UUID;
import org.fundacion_jala.notifications_service.models.Notification;

public interface NotificationWebService {
  Notification save(Notification notification);

  List<Notification> getAllByUserId(UUID userId);

  void send(Notification notification);
}

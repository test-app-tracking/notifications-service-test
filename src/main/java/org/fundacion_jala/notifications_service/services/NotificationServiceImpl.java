package org.fundacion_jala.notifications_service.services;

import java.util.List;
import java.util.UUID;
import org.fundacion_jala.notifications_service.models.Notification;
import org.fundacion_jala.notifications_service.repositories.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class NotificationServiceImpl implements NotificationWebService {

  private static final String NOTIFICATION_USERS_ROUTE = "/user/%s/queue/notification";

  @Autowired NotificationRepository notificationRepository;

  @Autowired private SimpMessagingTemplate messagingTemplate;

  @Override
  public Notification save(Notification notification) {
    return notificationRepository.save(notification);
  }

  @Override
  public List<Notification> getAllByUserId(UUID userId) {
    return notificationRepository.findByUserIdOrderByCreatedAtDesc(userId);
  }

  @Override
  public void send(Notification notification) {
    Notification notificationSaved = save(notification);
    String destiny = String.format(NOTIFICATION_USERS_ROUTE, notification.getUserId().toString());
    messagingTemplate.convertAndSend(destiny, notificationSaved);
  }
}

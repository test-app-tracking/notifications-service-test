package org.fundacion_jala.notifications_service.models;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import org.fundacion_jala.notifications_service.utils.constants.TableConstants;
import org.hibernate.annotations.Type;

@Getter
@Setter
@Entity
public class Notification {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Type(type = TableConstants.TYPE_UUID_CHAR)
  private UUID id;

  @Type(type = TableConstants.TYPE_UUID_CHAR)
  private UUID userId;

  @Size(min = 2, max = 100)
  private String title;

  @Size(min = 2, max = 255)
  private String description;

  private String type;

  private boolean isRead;

  @Column(nullable = false)
  private Date createdAt = new Date();

  public Notification() {}

  public Notification(UUID userId, String title, String description, String type) {
    this.id = UUID.randomUUID();
    this.userId = userId;
    this.title = title;
    this.description = description;
    this.type = type;
    this.isRead = false;
    createdAt = new Date();
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, title, type, isRead, createdAt);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    Notification notification = (Notification) obj;
    return id.equals(notification.getId())
        && title.equals(notification.getTitle())
        && type.equals(notification.getType())
        && isRead == notification.isRead()
        && createdAt.equals(notification.getCreatedAt());
  }
}

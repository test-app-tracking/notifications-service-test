package org.fundacion_jala.notifications_service.models.enums;

public enum NotificationType {
  RESET_PASSWORD,
  EVENT_NOTIFICATION,
  GENERATED_DEFAULT_PASSWORD,
  NEW_USER_NOTIFICATION,
}

package org.fundacion_jala.notifications_service.models.enums;

public enum NotificationStatus {
  SENT_SUCCESSFULLY,
  SENT_WITH_ERRORS,
  FAILED,
  SAVED,
  PENDING
}

package org.fundacion_jala.notifications_service.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;
import org.fundacion_jala.notifications_service.models.enums.NotificationStatus;
import org.fundacion_jala.notifications_service.utils.constants.TableConstants;
import org.hibernate.annotations.Type;

@Getter
@Setter
@Entity
public class EmailNotificationDetails {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Type(type = TableConstants.TYPE_UUID_CHAR)
  private UUID id;

  private String target;

  @Enumerated(EnumType.STRING)
  private NotificationStatus status;

  private String message;

  @ManyToOne(fetch = FetchType.LAZY)
  @JsonBackReference
  private EmailNotification owner;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof EmailNotificationDetails)) {
      return false;
    }
    EmailNotificationDetails that = (EmailNotificationDetails) o;
    return id.equals(that.getId()) && target.equals(that.getTarget());
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, target);
  }
}

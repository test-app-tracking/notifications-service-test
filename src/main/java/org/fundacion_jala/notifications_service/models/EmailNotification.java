package org.fundacion_jala.notifications_service.models;

import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import org.fundacion_jala.notifications_service.models.enums.NotificationStatus;
import org.fundacion_jala.notifications_service.models.enums.NotificationType;
import org.fundacion_jala.notifications_service.models.kafka.UserDTO;
import org.fundacion_jala.notifications_service.utils.constants.TableConstants;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;

@Getter
@Setter
@Entity
@Table(name = TableConstants.EMAIL_NOTIFICATION_TABLE)
public class EmailNotification {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Type(type = TableConstants.TYPE_UUID_CHAR)
  private UUID id;

  @ElementCollection
  @CollectionTable(
      name = TableConstants.EMAIL_TARGETS_TABLE,
      joinColumns = @JoinColumn(name = TableConstants.EMAIL_NOTIFICATION_ID))
  @Column(name = TableConstants.EMAIL_TARGET_COLUMN)
  private Set<String> emailTargets = new HashSet<>();

  @OneToMany(mappedBy = TableConstants.EMAIL_NOTIFICATION_ENTITY, cascade = CascadeType.ALL)
  private Set<EmailNotificationDetails> notificationDetails = new HashSet<>();

  @Type(type = TableConstants.TYPE_UUID_CHAR)
  private UUID ownerId;

  @Email private String sender;

  @Size(max = 100)
  private String subject;

  @Size(max = 350)
  private String body;

  @Transient Set<UserDTO> newUsers;

  @Enumerated(EnumType.STRING)
  private NotificationType type;

  @Enumerated(EnumType.STRING)
  private NotificationStatus status;

  private boolean hasTemplate;

  @CreationTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  private Date startTime;

  @UpdateTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  private Date lastUpdatedTime;

  @Column(columnDefinition = TableConstants.COLUMN_DEFINITION_DELETED)
  private boolean deleted;

  public EmailNotification() {}

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EmailNotification that = (EmailNotification) o;
    return emailTargets.equals(that.emailTargets)
        && Objects.equals(sender, that.sender)
        && subject.equals(that.subject)
        && body.equals(that.body)
        && type == that.type
        && status == that.status;
  }

  @Override
  public int hashCode() {
    return Objects.hash(emailTargets, sender, subject, body, type, status);
  }
}

package org.fundacion_jala.notifications_service.models.kafka;

import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class NewUserNotificationDTO extends EmailNotificationMessage {

  Set<UserDTO> users;
}

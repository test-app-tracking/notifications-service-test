package org.fundacion_jala.notifications_service.models.kafka;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import java.util.Set;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.fundacion_jala.notifications_service.utils.constants.JsonConstants;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = JsonConstants.PROPERTY_NAME)
@JsonSubTypes({
  @Type(value = NewUserNotificationDTO.class, name = JsonConstants.NAME_NEW_USERS),
})
public abstract class EmailNotificationMessage {

  private UUID ownerId;
  private String subject;
  private Set<String> emailTargets;
}

package org.fundacion_jala.notifications_service.configurations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.*;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

  @Value("${react.origin.url}")
  private String urlReactOrigin;

  private static final String NOTIFICATIONS_ENDPOINT = "/notifications";

  /**
   * Register the "/notifications" STOMP endpoint, enabling the SockJS protocol. SockJS is used
   * (both client and server side) to allow alternative messaging options if WebSocket is not
   * available
   */
  @Override
  public void registerStompEndpoints(StompEndpointRegistry registry) {
    registry.addEndpoint(NOTIFICATIONS_ENDPOINT).setAllowedOrigins(urlReactOrigin).withSockJS();
  }
}

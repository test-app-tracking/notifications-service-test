package org.fundacion_jala.notifications_service.configurations.kafka_config;

import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.fundacion_jala.notifications_service.models.kafka.EmailNotificationDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

@EnableKafka
@Configuration
public class KafkaConsumerConfig {

  @Value("${spring.kafka.bootstrap-servers}")
  private String kafkaConsumerPort;

  @Value("${spring.kafka.consumer.group-id}")
  private String kafkaConsumerGroupId;

  @Value("${spring.kafka.consumer.trusted-packages}")
  private String kafkaConsumerTrustedPackages;

  @Bean
  public ConsumerFactory<String, EmailNotificationDTO> consumerFactory() {
    Map<String, Object> config = new HashMap<>();
    config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaConsumerPort);
    config.put(ConsumerConfig.GROUP_ID_CONFIG, kafkaConsumerGroupId);
    config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
    config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);

    JsonDeserializer jsonDeserializer = new JsonDeserializer<>(EmailNotificationDTO.class);
    jsonDeserializer.addTrustedPackages(kafkaConsumerTrustedPackages);
    return new DefaultKafkaConsumerFactory<>(config, new StringDeserializer(), jsonDeserializer);
  }

  @Bean
  public ConcurrentKafkaListenerContainerFactory<String, EmailNotificationDTO>
      emailResetProcessListener() {
    ConcurrentKafkaListenerContainerFactory<String, EmailNotificationDTO> factory =
        new ConcurrentKafkaListenerContainerFactory<>();
    factory.setConsumerFactory(consumerFactory());
    return factory;
  }
}

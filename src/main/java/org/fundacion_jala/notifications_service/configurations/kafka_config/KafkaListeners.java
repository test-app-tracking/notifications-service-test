package org.fundacion_jala.notifications_service.configurations.kafka_config;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import javax.mail.MessagingException;
import org.fundacion_jala.notifications_service.configurations.kafka_config.helpers.CreateNewUserNotification;
import org.fundacion_jala.notifications_service.models.EmailNotification;
import org.fundacion_jala.notifications_service.models.enums.NotificationType;
import org.fundacion_jala.notifications_service.models.kafka.EmailNotificationDTO;
import org.fundacion_jala.notifications_service.models.kafka.NewUserNotificationDTO;
import org.fundacion_jala.notifications_service.services.NotificationService;
import org.fundacion_jala.notifications_service.utils.constants.EmailConstants;
import org.fundacion_jala.notifications_service.utils.constants.KafkaTopics;
import org.fundacion_jala.notifications_service.utils.exceptions.NotificationNotSentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaListeners {

  @Autowired private NotificationService<EmailNotification> emailService;

  @KafkaListener(
      topics = "${spring.kafka.consumer.topic-name}",
      groupId = "${spring.kafka.consumer.group-id}",
      containerFactory = "${spring.kafka.consumer.listener.container-factory}")
  void listen(EmailNotificationDTO emailNotificationDTO) {

    EmailNotification emailNotification = new EmailNotification();
    emailNotification.setSubject(emailNotificationDTO.getSubject());
    Set<String> emailTargets = new HashSet<>();
    emailTargets.add(emailNotificationDTO.getEmail());
    emailNotification.setEmailTargets(emailTargets);
    emailNotification.setBody(emailNotificationDTO.getBody());
    emailNotification.setOwnerId(UUID.fromString(emailNotificationDTO.getOwner()));
    emailNotification.setType(NotificationType.RESET_PASSWORD);
    emailNotification.setHasTemplate(true);

    try {
      emailService.send(emailNotification);
    } catch (MessagingException exception) {
      throw new NotificationNotSentException(EmailConstants.EMAIL_NOTIFICATION);
    }
  }

  @KafkaListener(
      topics = KafkaTopics.ADD_NEW_USERS,
      groupId = "${spring.kafka.consumer.group-id}",
      containerFactory = KafkaTopics.KAFKA_CONTAINER_FACTORY)
  public void listenAddUsers(NewUserNotificationDTO notification) {
    try {
      emailService.send(CreateNewUserNotification.getEmailNotification(notification));
    } catch (MessagingException exception) {
      throw new NotificationNotSentException(EmailConstants.EMAIL_NOTIFICATION);
    }
  }
}

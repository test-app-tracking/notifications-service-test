package org.fundacion_jala.notifications_service.configurations.kafka_config.helpers;

import org.fundacion_jala.notifications_service.models.EmailNotification;
import org.fundacion_jala.notifications_service.models.enums.NotificationType;
import org.fundacion_jala.notifications_service.models.kafka.NewUserNotificationDTO;
import org.fundacion_jala.notifications_service.utils.constants.EmailConstants;

public class CreateNewUserNotification {

  public static EmailNotification getEmailNotification(
      NewUserNotificationDTO newUsersNotification) {
    EmailNotification emailNotification = new EmailNotification();
    emailNotification.setSubject(newUsersNotification.getSubject());
    emailNotification.setEmailTargets(newUsersNotification.getEmailTargets());
    emailNotification.setOwnerId(newUsersNotification.getOwnerId());
    emailNotification.setType(NotificationType.NEW_USER_NOTIFICATION);
    emailNotification.setNewUsers(newUsersNotification.getUsers());
    emailNotification.setHasTemplate(true);
    emailNotification.setBody(EmailConstants.NEW_USERS_BODY);
    return emailNotification;
  }
}

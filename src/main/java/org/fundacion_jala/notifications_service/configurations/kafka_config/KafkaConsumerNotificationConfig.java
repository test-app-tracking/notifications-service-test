package org.fundacion_jala.notifications_service.configurations.kafka_config;

import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.fundacion_jala.notifications_service.models.kafka.EmailNotificationMessage;
import org.fundacion_jala.notifications_service.utils.exceptions.DeserializationExceptionHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

@EnableKafka
@Configuration
public class KafkaConsumerNotificationConfig {

  @Value("${spring.kafka.bootstrap-servers}")
  private String kafkaConsumerPort;

  @Value("${spring.kafka.consumer.group-id}")
  private String kafkaConsumerGroupId;

  public Map<String, Object> consumerConfig() {
    Map<String, Object> config = new HashMap<>();
    config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaConsumerPort);
    config.put(ConsumerConfig.GROUP_ID_CONFIG, kafkaConsumerGroupId);
    config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
    config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
    return config;
  }

  @Bean
  public ConsumerFactory<String, EmailNotificationMessage> notificationConsumerFactory() {
    JsonDeserializer jsonDeserializer = new JsonDeserializer<>(EmailNotificationMessage.class);
    return new DefaultKafkaConsumerFactory<>(
        consumerConfig(), new StringDeserializer(), jsonDeserializer);
  }

  @Bean
  public ConcurrentKafkaListenerContainerFactory<String, EmailNotificationMessage>
      notificationContainerFactory() {
    ConcurrentKafkaListenerContainerFactory<String, EmailNotificationMessage> factory =
        new ConcurrentKafkaListenerContainerFactory<>();
    factory.setConsumerFactory(notificationConsumerFactory());
    factory.setErrorHandler(new DeserializationExceptionHandler());
    return factory;
  }
}

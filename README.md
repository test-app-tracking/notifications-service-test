# tracking-tool-notifications-service

This microservice is responsible for sending and getting notifications via email

a. Notification

- POST http://localhost:8085/api/v1/notifications/notify
- GET http://localhost:8085/api/v1/notifications/user/{userId}

b. Email notification

- GET http://localhost:8085/api/v1/notifications/{notificationId}
- GET http://localhost:8085/api/v1/notifications/email
- POST http://localhost:8085/api/v1/notifications/email

go to http://localhost:8085/swagger-ui/#/Notification for watch the documentation of the
notifications service api

Database

- notificationsdb
		- tables:
				- email_notification
				- email_notification_details
				- email_targets
				- notification

The database tables are built with the models, there is a script for the db identity-service, in
the path: identity_service\src\main\resources\scripts\permissions\data.sql

#### References:

- https://docs.spring.io/spring-framework/docs/3.2.x/spring-framework-reference/html/mail.html
- https://cloud.spring.io/spring-cloud-netflix/multi/multi_spring-cloud-eureka-server.html
- https://cloud.spring.io/spring-cloud-netflix/multi/multi__service_discovery_eureka_clients.html
